unit uProcessWrap;

interface

uses
  Classes, SysUtils;

type

  { TRenderBuffer }

  TRenderBuffer = class
    constructor Create(SqrN: integer); virtual; abstract;
    procedure ToGPU(fromdata: Pointer); virtual; abstract;
    procedure FromGPU(todata: Pointer); virtual; abstract;
    procedure ActivateReading; virtual; abstract;
    procedure ActivateFBReading; virtual; abstract;
    procedure ActivateWriting; virtual; abstract;
  end;

  {

  TTestBuffer = class(TRenderBuffer)
    constructor Create(SqrN:Integer);override;
    procedure ToGPU(fromdata: Pointer);override;
    procedure FromGPU(todata: Pointer);override;
    procedure ActivateReading;override;
    procedure ActivateFBReading;override;
    procedure ActivateWriting;override;
  end;
   }

const
  TrivialPoly: array[1..8] of single = (0, 0, 0, 1, 1, 0, 1, 1);

//procedure DoBufferProcess;

implementation



end.
