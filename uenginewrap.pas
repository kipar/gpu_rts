unit uenginewrap;

interface

uses gl, Classes, SysUtils, uGame, uEngine, Resources;

const
  THE_SQRN = 256;

procedure PROCESS;
procedure RENDER;
procedure INIT;
procedure DONE;
procedure MOUSEUP(Button: byte; x, y: single);
procedure MOUSEDOWN(Button: byte; x, y: single);
procedure MOUSEMOVE(X, Y: integer);

function XY2Our(inx, iny: single; out X, Y: single): boolean;
function XSize: integer;
function YSize: integer;
function CurRate(side: integer; typ: integer): single;

procedure ShowStats;
procedure ProcessScroll;


var
  Game: TGameData;
  N1, N2: integer;
  choosemode: boolean;


implementation

uses Math;

function XSize: integer;
begin
  Result := EngineGet(Width);
end;

function YSize: integer;
begin
  Result := EngineGet(Height);
end;

function CurRate(side: integer; typ: integer): single;
begin
  Result := Game.Results[side].ByType[typ, 1] / ifthen(side = 1, N1, N2) * Game.N;
end;

procedure ShowStats;
var
  i, y: integer;
  ax, ay: single;
  w: integer = 50;
begin
  SetLayer(201);
  y := 0;
  for i := 1 to 8 do
  begin
    if CurRate(1, i) = 0 then
      continue;
    rect(100, y * w, CurRate(1, i) * XSize / 2, w * 0.9, True, GREEN);
    DrawText(RES.Consola, PChar(IntToStr(Trunc(Game.N * Game.Results[1].ByType[i, 1]))),
      100, y * w + 5);
    DrawTiled(RES.Ship_icons, i - 1, w, y * w + w / 2, 0.5, 0.5);
    Inc(y);
  end;

  y := 0;
  for i := 1 to 8 do
  begin
    if CurRate(2, i) = 0 then
      continue;
    rect(XSize - CurRate(2, i) * XSize / 2 - 100, y * w, CurRate(2, i) * XSize / 2, w * 0.9, True, RED);
    DrawTiled(RES.Ship_icons, i - 1, XSize - w, y * w + w / 2, 0.5, 0.5);
    DrawText(RES.Consola, PChar(IntToStr(Trunc(Game.N * Game.Results[2].ByType[i, 1]))),
      XSize - 200, y * w + 5);
    Inc(y);
  end;
  XY2Our(MouseGet(CursorX), MouseGet(CursorY), ax, ay);
  DrawText(RES.Consola, PChar(Format('%f, %f', [ax, ay])), XSize / 2, YSize * 0.9);
end;

procedure ProcessScroll;
begin
  if (KeyState(KeyLeft) <> ksUp) or (KeyState(KeyA) <> ksUp) then
    Game.Center.X := Game.Center.X - Game.Scale / 60;
  if (KeyState(KeyRight) <> ksUp) or (KeyState(KeyD) <> ksUp) then
    Game.Center.X := Game.Center.X + Game.Scale / 60;
  if (KeyState(KeyUp) <> ksUp) or (KeyState(KeyW) <> ksUp) then
    Game.Center.Y := Game.Center.Y + Game.Scale / 60;
  if (KeyState(KeyDown) <> ksUp) or (KeyState(KeyS) <> ksUp) then
    Game.Center.Y := Game.Center.Y - Game.Scale / 60;
end;

function XY2Our(inx, iny: single; out X, Y: single): boolean;
begin
  Result := True;
  X := Game.Center.X + (inx - XSize / 2) / YSize * Game.Scale;
  Y := Game.Center.Y + (YSize / 2 - iny) / YSize * Game.Scale;
end;

procedure PROCESS;
begin
  Game.AllPasses;
  if Game.Results[1].Total[1] <= 0.01 * N1 / Game.N then
  begin
    log('LOST');
    Game.NewBattle(Game.army_file, N1, N2);
  end;
  if Game.Results[2].Total[1] <= 0.01 * N2 / Game.N then
  begin
    log('WIN');
    if N2 < 32000 then
    begin
      N1 := Trunc(N1 * 1.9);
      N2 := N2 * 2;
    end;
    Game.NewBattle(Game.army_file, N1, N2);
  end;
end;

procedure RENDER;
begin
  Game.RenderPass;
end;

procedure INIT;
begin
  N1 := 2000;
  N2 := 2000;
  Randomize;
  glPointSize(1);
  glDisable(GL_DEPTH_TEST);
  Game := TGameData.Create(THE_SQRN, XSize, YSize);
  Game.NewBattle(Game.army_file, N1, N2);
end;

procedure DONE;
begin
  // Insert user code here
end;

procedure MOUSEDOWN(Button: byte; x, y: single);
var
  dx, dy: single;
begin
  XY2Our(X, Y, dX, dY);
  case Button of
    1:
    begin
      Game.mousedown := True;
      Game.clicktime := GetTickCount;
      Game.Command(Start, dx, dy);
    end;
    3: choosemode := True;
  end;
end;

procedure MOUSEUP(Button: byte; x, y: single);
var
  dx, dy: single;
begin
  XY2Our(X, Y, dX, dY);
  Game.mousedown := False;
  case Button of
    1:
    begin
      Game.Command(Update, dx, dy);
      Game.Command(Commit, dx, dy);
    end;
    2: Game.Command(Delete, dx, dy);
    3: if choosemode then
      begin
        Game.SelectChoosen;
        choosemode := False;
      end;

  end;
end;

procedure MOUSEMOVE(X, Y: integer);
var
  dx, dy: single;
begin
  XY2Our(X, Y, dX, dY);
  Game.Cursor.X := dx;
  Game.Cursor.Y := dy;

  //Game.clickX := Game.clickX + dx * Game.Scale;
  //Game.clickY := Game.clickY - dy * Game.Scale;
  if choosemode then
  begin
    if Game.UnderCursor > 0 then
    begin
      Game.SelectChoosen;
      choosemode := False;
    end;
  end;
  //else
  //  Game.Command(Update, Game.clickX, Game.clickY);

end;

end.
