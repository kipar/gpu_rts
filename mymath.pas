unit MyMath;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF}

interface

uses SysUtils, Classes, uEngine, Resources;

type
  { TRawResourceStream }

  TRawResourceStream = class(TCustomMemoryStream)
    constructor Create(res: TRawResource);
  end;


implementation

{ TRawResourceStream }

constructor TRawResourceStream.Create(res: TRawResource);
var
  asize: integer;
  ptr: Pointer;
begin
  ptr := RawResource(res, asize);
  SetPointer(ptr, asize);
end;


end.
