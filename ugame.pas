unit uGame;

interface

uses
  Classes, SysUtils, gl, uEngine, uTextures,
  IniFiles, MyMath, Resources, Types;

const

  CHECK_PIECE = 1000;
  NWEAPONS = 10;
  NTYPES = 8;
  BOARD_WEAPONS = 8;

  NCOMMANDS = 8;

  STATIC_SIZE = 128;

  N_STAT_PASSES = 5;

type

  TPlayer = 1..2;

  { TGameData }

  TShipData = packed record
    X, Y, HP, Typ, VX, VY, Target, Angle: single;
  end;


  TWeapon = packed record
    Damage, Burst, Reload, Range: single;
    Color: array[1..3] of single;
    ShotType: single;
  end;

  TShipType = packed record
    Size, Thrust, TurnSpeed, AttackAngle,
    MaxHP, Regen, MaxShield, ShieldRegen: single;
    Weapons: array[1..BOARD_WEAPONS] of single;
  end;


  TCommand = packed record
    start, finish: array[1..2] of single;
    time: single;
  end;

  TStats = packed record
    ByType: array[1..NTYPES, 1..4] of single;
    ByOrder: array[1..NCOMMANDS, 1..4] of single;
    Total: array[1..4] of single;
    Selected: array[1..4] of single;
    Choosen: array[1..4] of single;
    filler: array[1..(8 * 8 - NTYPES * 2 - 3) * 4] of single;
  end;

  { TStaticData }

  TStaticData = packed object
    TypeData: array[1..NTYPES] of TShipType;
    WeaponData: array[1..NWEAPONS] of TWeapon;
    RangesData: array[1..NTYPES, 1..NTYPES] of single;
    Dummy: array[1..17000] of single;
    procedure FromFiles(WeaponFile, ShipFile: TCustomIniFile);
  end;

  { TArmies }

  TArmies = packed object
    Data: array[TPlayer, 1..NTYPES] of single;
    procedure FromFile(armyfile: TCustomIniFile);
    function GetType(p: TPlayer): integer;
  end;

  TCommandAction = (Start, Commit, Update, Delete);


  TGameData = class
    N, SqrN: integer;
    QuadPoints, RenderPoints, RenderLines, ProcessPoints, CommandsList: TVertexList;
    Stats, ShipsState, NewShipsState, ShipsDamage{, ShipsPosition}: array[1..2] of TTexture;
    Stars: TTexture;
    StartData: array[1..2] of array of TShipData;
    StaticData: TStaticData;
    Commands: array[1..2, 1..NCOMMANDS] of TCommand;
    Statics: TTexture;
    CurOffset: integer;
    Center, Cursor: TPointF;
    Scale, aspect: single;
    ChoosenPos, Speed: TPointF;
    ChoosenFirst: boolean;
    mousedown: boolean;
    clicktime: integer;
    MapSizeX, MapSizeY: integer;
    frame: integer;
    should_update: array[1..2] of integer;
    field_x: integer;
    field_y: integer;

    sunpos: array[1..2] of single;
    sunlight: array[1..3] of single;
    Results: array[TPlayer] of TStats;

    army_file, ships_file, weapons_file: TIniFile;

    UnderCursor, Choosen: integer;

    constructor Create(aSQRN, aMapSizeX, aMapSizeY: integer);
    procedure NewBattle(thefile: TCustomIniFile; n1, n2: integer);
    procedure StatsPass;
    procedure AllPasses;
    procedure RenderPass;
    procedure CommonUniforms;

    procedure Command(Action: TCommandAction; X, Y: single);
    procedure CommandAI;

    procedure SelectChoosen;
    procedure DeselectChoosen;
    procedure ProcessChoosen(x, y: single);

  end;

const
  CommandColors: array[1..NCOMMANDS + 1, 1..4] of single = (
    (0, 0, 0, 0),
    (1, 0.5, 0, 1),
    (0, 1, 0, 1),
    (0, 0, 1, 1),
    (0.5, 1, 0, 1),
    (0.5, 0, 0.5, 1),
    (0, 1, 1, 1),
    (1, 1, 1, 1),
    (0.5, 0.5, 0.5, 1)
    );

implementation

uses uDist;

function PointF(x, y: single): TPointF;
begin
  Result.x := x;
  Result.y := y;
end;



{ TArmies }

procedure TArmies.FromFile(armyfile: TCustomIniFile);
var
  I, J: integer;
begin
  for I := 1 to 2 do
    for J := 1 to NTYPES do
      Data[I, J] := armyfile.ReadFloat(IntToStr(I), IntToStr(J), 0);
  Data[1, NTYPES] := 10;
  Data[2, NTYPES] := 10;
end;

function TArmies.GetType(p: TPlayer): integer;
var
  roll: single;
begin
  roll := random;
  Result := 1;
  while roll > Data[p, Result] do
  begin
    roll -= Data[p, Result];
    Inc(Result);
  end;
end;

{ TStaticData }

procedure TStaticData.FromFiles(WeaponFile, ShipFile: TCustomIniFile);
var
  I, J, K, W, First, last, maxr: integer;
  weapon: ^TWeapon;
  xx: array[1..1000] of single;
  best: single;
begin
  for I := 1 to NWEAPONS do
    with WeaponData[i] do
    begin
      Damage := WeaponFile.ReadFloat(IntToStr(I), 'Damage', 0);
      Burst := WeaponFile.ReadFloat(IntToStr(I), 'Burst', 0);
      Reload := WeaponFile.ReadFloat(IntToStr(I), 'Reload', 0);
      Range := WeaponFile.ReadFloat(IntToStr(I), 'Range', 0);
      ShotType := WeaponFile.ReadFloat(IntToStr(I), 'ShotType', 0);
      Color[1] := WeaponFile.ReadFloat(IntToStr(I), 'ColorR', 0);
      Color[2] := WeaponFile.ReadFloat(IntToStr(I), 'ColorG', 0);
      Color[3] := WeaponFile.ReadFloat(IntToStr(I), 'ColorB', 0);
    end;
  for I := 1 to NTYPES do
    with TypeData[i] do
    begin
      Size := ShipFile.ReadFloat(IntToStr(I), 'Size', 0);
      MaxHP := ShipFile.ReadFloat(IntToStr(I), 'MaxHP', 0);
      Regen := ShipFile.ReadFloat(IntToStr(I), 'Regen', 0);
      MaxShield := ShipFile.ReadFloat(IntToStr(I), 'MaxShield', 0);
      ShieldRegen := ShipFile.ReadFloat(IntToStr(I), 'ShieldRegen', 0);
      Thrust := ShipFile.ReadFloat(IntToStr(I), 'Thrust', 0) * 3;
      TurnSpeed := ShipFile.ReadFloat(IntToStr(I), 'TurnSpeed', 0);
      AttackAngle := ShipFile.ReadFloat(IntToStr(I), 'AttackAngle', 0);
      for J := 1 to BOARD_WEAPONS do
        Weapons[J] := ShipFile.ReadFloat(IntToStr(I), 'Weapon' + IntToStr(J), 0) - 1;
    end;

  //now calculate optimal distances for ship i against ship j
  for I := 1 to NTYPES do
    for J := 1 to NTYPES do
    begin
      maxr := 1;
      for W := 1 to BOARD_WEAPONS do
        if TypeData[I].Weapons[W] > 0 then
        begin
          weapon := @WeaponData[Trunc(TypeData[I].Weapons[W])];
          if maxr < weapon^.Range then
            maxr := Trunc(weapon^.Range);
        end;
      for K := 1 to maxr do
      begin
        xx[K] := 0;
        for W := 1 to BOARD_WEAPONS do
        begin
          if TypeData[I].Weapons[W] > 0 then
          begin
            weapon := @WeaponData[Trunc(TypeData[I].Weapons[W])];
            if K < weapon^.Range then
              xx[K] += weapon^.Burst * weapon^.Damage / weapon^.Reload;
          end;
          if (I <> J) and (TypeData[J].Weapons[W] > 0) then
          begin
            weapon := @WeaponData[Trunc(TypeData[J].Weapons[W])];
            if K < weapon^.Range then
              xx[K] -= weapon^.Burst * weapon^.Damage / weapon^.Reload;
          end;
        end;
      end;
      //find best value
      best := -1000000;
      First := 1;
      last := maxr;
      for K := 1 to maxr do
        if best < xx[K] then
        begin
          best := xx[K];
          First := K;
        end;
      //find when it ends
      for K := First to maxr do
        if xx[K] < best then
        begin
          last := K - 1;
          break;
        end;
      //so, optimal distance
      if First <= 1 then
        RangesData[I, J] := 1
      else if last >= maxr - 1 then
        RangesData[I, J] := last - (last - First) / 5
      else
        RangesData[I, J] := (First + last) / 2;
      //log(Format('%d vs %d = %f',[I,J,RangesData[I, J]]));
    end;
end;


var
  BigList: array of Single;

{ TGameData }

constructor TGameData.Create(aSQRN, aMapSizeX, aMapSizeY: integer);
var
  i: integer;
begin
  SetLength(BigList, aSQRN*(aSQRN div 2)*4*6);
  for i := 0 to Length(BigList)-1 do
    BigList[i] := I;
  army_file := TIniFile.Create(TRawResourceStream.Create(RES.Data.Army_dat));
  ships_file := TIniFile.Create(TRawResourceStream.Create(RES.Data.Ships_dat));
  weapons_file := TIniFile.Create(TRawResourceStream.Create(RES.Data.Weapons_dat));
  MapSizeX := aMapSizeX;
  MapSizeY := aMapSizeY;
  StaticData.FromFiles(weapons_file, ships_file);
  N := aSQRN * aSQRN div 2;
  SqrN := aSQRN;
  QuadPoints := VertexListCreate(@BigList[0], vlTriangles, sizeof(Single), 6);
  VertexListAddField(QuadPoints, attr_dummy);
  ProcessPoints := VertexListCreate(@BigList[0], vlPoints, sizeof(Single), N);
  VertexListAddField(ProcessPoints, attr_dummy);
  RenderPoints := VertexListCreate(@BigList[0], vlTriangles, sizeof(Single), N * 6);
  VertexListAddField(RenderPoints, attr_dummy);
  RenderLines := VertexListCreate(@BigList[0], vlTriangles, sizeof(Single), N * 4{BOARD_WEAPONS} * 6);
  VertexListAddField(RenderLines, attr_dummy);
  CommandsList := VertexListCreate(@BigList[0], vlTriangles, sizeof(Single), NCOMMANDS * 6);
  VertexListAddField(CommandsList, attr_dummy);
  Stars := TTexture.CreateStars;

  Statics := TTexture.Create(STATIC_SIZE);
  Statics.ToGPU(@StaticData);

  //glBlendFunc(GL_ONE, GL_ONE);
  //glBlendEquation(GL_FUNC_ADD);
end;

procedure TGameData.NewBattle(thefile: TCustomIniFile; n1, n2: integer);
var
  p: TPlayer;
  i: integer;
  armies: TArmies;
  nn: integer;

begin
  logf('Starting battle: %d vs %d', [n1, n2]);
  field_x := (n1 + n2) div 5;
  field_y := (n1 + n2) div 10;
  CurOffset := 0;
  UnderCursor := -1;
  Speed := PointF(0, 0);
  Choosen := -1;
  Scale := field_y * 4;
  aspect := MapSizeX / MapSizeY;
  Center := PointF(0, 0);
  for P := 1 to 2 do
    for I := 1 to NCOMMANDS do
      Commands[p, i].time := -1;

  if random < 0.5 then
    sunpos[1] := random * field_x - field_x * 1.5
  else
    sunpos[1] := random * field_x + field_x * 0.5;

  sunpos[2] := random * field_y * 2 - field_y;
  for I := 1 to 3 do
    sunlight[I] := 1 - random / 2;

  armies.FromFile(thefile);

  for p in TPlayer do
  begin
    if p = 1 then
      nn := n1
    else
      nn := n2;
    Stats[p] := TTexture.Create(8);
    ShipsState[p] := TTexture.Create(SqrN);
    NewShipsState[p] := TTexture.Create(SqrN);
    ShipsDamage[p] := TTexture.Create(SqrN);
    //    ShipsPosition[p] := TTexture.Create(max(MapSizeX, MapSizeY), True);

    SetLength(StartData[p], N);
    for i := 0 to N - 1 do
      with StartData[p][i] do
      begin
        VX := (random - 0.5) / 50;
        VY := (random - 0.5) / 50;
        Target := 0;//random(N);
        Typ := armies.GetType(p) - 1;

        if i < nn then
          HP := StaticData.TypeData[Trunc(Typ) + 1].MaxHP * 256 +
            StaticData.TypeData[Trunc(Typ) + 1].MaxShield / 64
        else
          HP := -1000;

        X := -field_x / 2 + random * field_x;

        Y := field_y + random * field_y + (NTypes - Typ) * 50;
        if p = 1 then
          y := -Y
        else
          y := +Y;

        if p = 1 then
          Angle := pi / 2
        else
          Angle := -pi / 2;
      end;
    ShipsState[p].ToGPU(Pointer(StartData[p]));
    NewShipsState[p].ToGPU(Pointer(StartData[p]));
  end;
end;

procedure TGameData.StatsPass;
var
  p: TPlayer;
  pass: integer;
begin
  UniformSetInt(RES.Shaders.Stats, uni_choosen, Choosen);
  ShaderActivate(RES.Shaders.Stats);
  glEnable(GL_BLEND);
  for p in TPlayer do
  begin
    ShipsState[p].ActivateReading(RES.Shaders.Stats, uni_texdata);
    Stats[p].ActivateWriting;
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    for pass := 1 to N_STAT_PASSES do
    begin
      UniformSetInt(RES.Shaders.Stats, uni_pass, pass);
      if pass > 3 then
      begin
        glDisable(GL_BLEND);
      end;
      VertexListDraw(ProcessPoints, -1, false);
      if pass > 3 then
      begin
        glEnable(GL_BLEND);
      end;
    end;
    Stats[p].FromGPU(@Results[p]);
  end;
  UnderCursor := Trunc(Results[1].Selected[1]);
  if Results[1].Choosen[3] < 0 then
    DeselectChoosen;
  ProcessChoosen(Results[1].Choosen[1], Results[1].Choosen[2]);
  //logf('(%d)%f vs %f', [UnderCursor, Results[1].Total[1]*SqrN*SqrN/2, Results[2].Total[1]*SqrN*SqrN/2]);
end;

procedure TGameData.AllPasses;
var
  p: TPlayer;
begin
  Inc(frame);
  //glActiveTexture(GL_TEXTURE8);
  //Statics.ActivateReadingOld;
  Statics.ActivateReading(ALL_SHADERS, uni_statics);


  StatsPass;

  glEnable(GL_BLEND);
  ShaderActivate(RES.Shaders.Damage);
  for p in TPlayer do
  begin
    NewShipsState[p].ActivateReading(RES.Shaders.Damage, uni_texdata);
    NewShipsState[3 - p].ActivateReading(RES.Shaders.Damage, uni_enemies);

    ShipsDamage[3 - p].ActivateWriting;
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    VertexListDraw(ProcessPoints, -1, false);
  end;
  glDisable(GL_BLEND);


  UniformSetInt(RES.Shaders.Select_target, uni_check_offset, CurOffset);
  ShaderActivate(RES.Shaders.Select_target);
  for p in TPlayer do
  begin
    UniformSetInt(RES.Shaders.Select_target, uni_order_side, p);
    if should_update[p] > 0 then
    begin
      UniformSetFloat(RES.Shaders.Select_target, uni_update_order, should_update[p]);
      should_update[p] := 0;
    end
    else
      UniformSetFloat(RES.Shaders.Select_target, uni_update_order, 0);
    ShipsDamage[p].ActivateReading(RES.Shaders.Select_target, uni_damage);
    NewShipsState[p].ActivateReading(RES.Shaders.Select_target, uni_texdata);
    NewShipsState[3 - p].ActivateReading(RES.Shaders.Select_target, uni_enemies);


    ShipsState[p].ActivateWriting;
    glClear(GL_COLOR_BUFFER_BIT);
    VertexListDraw(QuadPoints, -1, false);
  end;
  Inc(CurOffset, CHECK_PIECE);
  if CurOffset >= N then
    CurOffset := 0
  else if CurOffset > N - CHECK_PIECE then
    CurOffset := N - CHECK_PIECE;


  ShaderActivate(RES.Shaders.Integrate);
  for p in TPlayer do
  begin
    UniformSetInt(RES.Shaders.Integrate, uni_side, p);
    ShipsState[p].ActivateReading(RES.Shaders.Integrate, uni_texdata);
    ShipsState[3 - p].ActivateReading(RES.Shaders.Integrate, uni_enemies);

    NewShipsState[p].ActivateWriting;
    glClear(GL_COLOR_BUFFER_BIT);
    VertexListDraw(QuadPoints, -1, false);
  end;

  CommandAI;
end;

procedure TGameData.RenderPass;
var
  p: TPlayer;
begin
  CommonUniforms;
  UniformSetPtr(RES.Shaders.Test_gui, uni_sunpos, @sunpos);
  UniformSetPtr(RES.Shaders.Test_gui, uni_sunlight, @sunlight);
  UniformSetTexture(RES.Shaders.Test_gui, uni_cursor, RES.Cursor);
  Stars.ActivateReading(RES.Shaders.Test_gui, uni_tex);
  ShaderActivate(RES.Shaders.Test_gui);
  glClear(GL_COLOR_BUFFER_BIT);
  VertexListDraw(QuadPoints, -1, false);

  glEnable(GL_BLEND);
  UniformSetTexture(RES.Shaders.Render_halo, uni_tex, RES.Ships);
  ShaderActivate(RES.Shaders.Render_halo);
  for p in TPlayer do
  begin
    UniformSetInt(RES.Shaders.Render_halo, uni_side, p);
    ShipsState[p].ActivateReading(RES.Shaders.Render_halo, uni_texdata);
    NewShipsState[p].ActivateReading(RES.Shaders.Render_halo, uni_prevdata);
    VertexListDraw(RenderPoints, -1, false);
  end;
  glDisable(GL_BLEND);

  UniformSetTexture(RES.Shaders.Render, uni_tex, RES.Ships);
  ShaderActivate(RES.Shaders.Render);
  for p in TPlayer do
  begin
    UniformSetInt(RES.Shaders.Render, uni_side, p);
    ShipsState[p].ActivateReading(RES.Shaders.Render, uni_texdata);
    VertexListDraw(RenderPoints, -1, false);
  end;

  ShaderActivate(RES.Shaders.Render_lines);
  for p in TPlayer do
  begin
    UniformSetInt(RES.Shaders.Render_lines, uni_side, p);
    ShipsState[p].ActivateReading(RES.Shaders.Render_lines, uni_texdata);
    ShipsState[3 - p].ActivateReading(RES.Shaders.Render_lines, uni_enemies);
    VertexListDraw(RenderLines, -1, false);
  end;

  UniformSetInt(RES.Shaders.Render_commands, uni_side, 1);
  UniformSetTexture(RES.Shaders.Render_commands, uni_arrow, RES.Arrow);
  ShaderActivate(RES.Shaders.Render_commands);
  VertexListDraw(CommandsList, -1, false);
end;

procedure TGameData.CommonUniforms;
begin
  UniformSetFloat(ALL_SHADERS, uni_time, frame mod 1000000);
  UniformSetFloat(ALL_SHADERS, uni_screen_scale, Scale);
  UniformSetFloat(ALL_SHADERS, uni_aspect, aspect);

  UniformSetPtr(ALL_SHADERS, uni_camera, @Center);
  UniformSetPtr(ALL_SHADERS, uni_mouse, @Cursor);

  UniformSetPtr(ALL_SHADERS, uni_commands, @Commands);
  UniformSetPtr(ALL_SHADERS, uni_cmd_colors, @CommandColors);
end;

procedure TGameData.Command(Action: TCommandAction; X, Y: single);
var
  I, J: integer;
  mint, dist: single;
begin
  case Action of
    Start:
    begin
      Commands[1, NCOMMANDS].start[1] := X;
      Commands[1, NCOMMANDS].start[2] := Y;
      Commands[1, NCOMMANDS].finish[1] := X;
      Commands[1, NCOMMANDS].finish[2] := Y;
      Commands[1, NCOMMANDS].time := frame;
    end;
    Update:
    begin
      Commands[1, NCOMMANDS].finish[1] := X;
      Commands[1, NCOMMANDS].finish[2] := Y;
    end;
    Commit:
    begin
      mint := frame;
      J := 1;
      for I := 1 to NCOMMANDS - 1 do
        if Commands[1, I].time < mint then
        begin
          J := I;
          mint := Commands[1, I].time;
        end;
      if should_update[1] > 0 then
        exit;//TODO -???
      should_update[1] := J;
      with Commands[1, J] do
      begin
        time := frame;
        start := Commands[1, NCOMMANDS].start;
        finish := Commands[1, NCOMMANDS].finish;
      end;
      Commands[1, NCOMMANDS].time := -1;
    end;
    Delete:
    begin
      if should_update[1] > 0 then
        exit;//TODO -???
      mint := 1e10;
      J := -1;
      for I := 1 to NCOMMANDS - 1 do
        if Commands[1, I].time >= 0 then
        begin
          dist := ShortestDistance2(x, y, Commands[1, I].start[1],
            Commands[1, I].start[2], Commands[1, I].finish[1], Commands[1, I].finish[2]);
          if dist < mint then
          begin
            J := I;
            mint := dist;
          end;
        end;
      if J < 0 then
        exit;
      should_update[1] := J;
      Commands[1, J].time := -1;
    end;
  end;
end;

procedure TGameData.CommandAI;
var
  I: integer;
begin
  if should_update[2] > 0 then
    exit;
  if random(500) = 1 then
  begin
    I := random(NCOMMANDS - 1) + 1;
    should_update[2] := I;
    if Commands[2, I].time > 0 then
      Commands[2, I].time := -1
    else
    begin
      Commands[2, I].time := frame;
      Commands[2, I].start[1] := random * field_x * 2 - field_x;
      Commands[2, I].start[2] := random * field_y * 2 - field_y;
      Commands[2, I].finish[1] := random * field_x * 2 - field_x;
      Commands[2, I].finish[2] := random * field_y * 2 - field_y;
    end;
  end;
end;

procedure TGameData.SelectChoosen;
begin
  if UnderCursor <= 0 then
    DeselectChoosen
  else
  begin
    ChoosenFirst := True;
    Choosen := UnderCursor;
    log('selected: ' + IntToStr(UnderCursor));
  end;
end;

procedure TGameData.DeselectChoosen;
begin
  log('deselected: ' + IntToStr(Choosen));
  ChoosenFirst := False;
  Choosen := -100;
end;

procedure TGameData.ProcessChoosen(x, y: single);
var
  p: TPointF;
begin
  p := PointF(x, y);
  if Choosen < 0 then
  begin
    Center := Center + Speed;
    exit;
  end;
  if ChoosenFirst then
  begin
    Center := p;
    Speed := PointF(0, 0);
  end
  else
  begin
    Speed := p - ChoosenPos;
    Center := Center + Speed;
  end;
  ChoosenFirst := False;
  ChoosenPos := p;
end;



end.
