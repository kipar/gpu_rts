unit uDist;

interface

uses
  Classes, SysUtils;

//taken from http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment/10640089#10640089
function ShortestDistance2(px, py, vx, vy, wx, wy: single): single;

implementation

uses Math;

type
  PointF = record
    x, y: single;
  end;

function pp(const x, y: single): PointF;
begin
  Result.X := x;
  Result.Y := y;
end;

function Dot(const p1, p2: PointF): single;
begin
  Result := p1.x * p2.x + p1.y * p2.y;
end;

function SubPoint(const p1, p2: PointF): PointF;
begin
  Result.x := p1.x - p2.x;
  Result.y := p1.y - p2.y;
end;

function Distance(const p1, p2: PointF): single;
begin
  Result := hypot(p1.x - p2.x, p1.y - p2.y);
end;

function SquaredDistance(const p1, p2: PointF): single;
begin
  Result := Sqr(p1.x - p2.x) + Sqr(p1.y - p2.y);
end;

function ShortestDistance2(px, py, vx, vy, wx, wy: single): single;
var
  l2, t: single;
  tt: PointF;
  p, v, w: PointF;
begin
  p := pp(px, py);
  v := pp(vx, vy);
  w := pp(wx, wy);
  // Return minimum distance between line segment vw and point p
  //l2 := length_squared(v, w);  // i.e. |w-v|^2 -  avoid a sqrt
  l2 := SquaredDistance(v, w);
  if (l2 = 0.0) then
  begin
    Result := Distance(p, v);   // v == w case
    exit;
  end;
  // Consider the line extending the segment, parameterized as v + t (w - v).
  // We find projection of point p onto the line.
  // It falls where t = [(p-v) . (w-v)] / |w-v|^2
  t := Dot(SubPoint(p, v), SubPoint(w, v)) / l2;
  if (t < 0.0) then
  begin
    Result := Distance(p, v);       // Beyond the 'v' end of the segment
    exit;
  end
  else if (t > 1.0) then
  begin
    Result := Distance(p, w);  // Beyond the 'w' end of the segment
    exit;
  end;
  //projection := v + t * (w - v);  // Projection falls on the segment
  tt.x := v.x + t * (w.x - v.x);
  tt.y := v.y + t * (w.y - v.y);
  Result := Distance(p, tt);
end;

end.
