
attribute float dummy;
varying float attack;
uniform sampler2D texdata;
uniform sampler2D enemies;

void main(void)
{
  vec4 data = texture2D(texdata, index2tex(dummy*2.0));
  vec2 target_angle = texture2D(texdata, index2tex(dummy*2.0+1.0)).zw;
  float target = floor(target_angle.x/MAX_COMMANDSf);
  vec3 enemypos = texture2D(enemies, index2tex(target*2.0)).xyz;
  attack = 0.0;
  float dist = length(enemypos.xy - data.xy);
  gl_Position = vec4(0,0,0,0);
  float wantangle = atan2(enemypos.xy - data.xy);
  float fire_angle = /*ATTACK_ANGLE*/shipdata(data.w, 0.0).w;

                                                                 //TODO
  if(data.z > 0.0 && enemypos.z > 0.0 && target >=0.0 && angle_dist(wantangle, target_angle.y) <= fire_angle)
  {
    vec4 weapons1 = shipdata(data.w, 2.0);
    vec2 seed = fract(enemypos.xy+data.yx + index2tex(dummy*2.0));
    for(int i=0; i<4; i++)
    if(weapons1[i] >= 0.0)
    {
      vec4 weapon = weapondata(weapons1[i], 0.0);
      if(dist < /*FIRE_DISTANCE*/weapon.w && check_reload(/*RELOAD*/weapon.z))
      {
        for(int j = 0; j<int(/*BURST*/weapon.y); j++)
          attack += step(rand(seed+vec2(float(i)/10.0,float(j)/10.0)) , /*DAMAGE*/weapon.x);
      }
    }
    if(attack > 0.0)
      gl_Position = abs_to_screen(index2tex(target*2.0)+vec2(0.5/SQRN));
    //gl_Position = abs_to_screen(index2tex(dummy*2.0+1.0));
  }
}
