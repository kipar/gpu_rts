include: consts.hs
vertex: damage.vs
fragment: damage.fs
uniform: enemies: sampler2D
uniform: statics: sampler2D
uniform: texdata: sampler2D
uniform: time: float
attribute: dummy: float
