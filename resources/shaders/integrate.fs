varying vec2 texcoord;
uniform sampler2D texdata;
uniform sampler2D enemies;
uniform int side;


vec2 ramp_decide(vec2 dist, vec2 relspeed, float thrust)
{
//thrust*=100.0;
  vec2 dstVelocity = (dist) * RAMP_K1*thrust;
  vec2 result =  (dstVelocity - relspeed)*RAMP_K2;
  if(length(result) > thrust)
    return normalize(result)*thrust;
  else
    return result;
//  return normalize(dist)*thrust;
  
}

void main(void)
{
  float is_second = mod(texcoord.x*SQRN,2.0);
  if(is_second > 0.9)
  {
    vec4 data = texture2D(texdata, texcoord);
    float target = floor(data.z/MAX_COMMANDSf);
    float cmd_id = mod(data.z, MAX_COMMANDSf);
    vec4 enemypos = texture2D(enemies, index2tex(target*2.0));
    vec4 ourpos = texture2D(texdata, texcoord-vec2(1.0/SQRN, 0.0));
    vec4 ourdata = shipdata(ourpos.w, 0.0);
    if(ourpos.z <= 0.0)
      gl_FragColor = data;
    else
    {
      vec2 decision;
      float wantangle;
      float order_weight;
      if(target < 0.0 /*|| enemypos.z <= 0.0*/)
      {
        //standing by
        //gl_FragColor = vec4(max(vec2(0), data.xy-/*THRUST*/ourdata.yy), data.zw);//max(vec2(0), data.w-/*TURN_SPEED*/ourdata.z));
        wantangle = pi_2;
        decision = normalize(-data.xy)*/*THRUST*/ourdata.y;
        order_weight = 1.0;
        //return;
      }
//      gl_FragColor = data;

      else{
        //moving to\from target ship
        vec2 relspeed = texture2D(enemies, index2tex(target*2.0+1.0)).xy - data.xy;
        //vec2 distance = enemypos.xy - ourpos.xy /*+ relspeed*(PREDICT_MIN+PREDICT_D*rand(texcoord))*/;// test***
        vec2 distance = enemypos.xy - ourpos.xy + relspeed*(PREDICT_MIN+PREDICT_D*rand(texcoord));
        float need_dist = optimal_range(ourpos.w, enemypos.w);
        float len_dist = length(distance);
        //decision = ramp_decide(normalize(distance)* (len_dist - need_dist), -relspeed, /*THRUST*/ourdata.y);// test***
        decision = normalize(distance) * sign(len_dist - need_dist)*/*THRUST*/ourdata.y;
        wantangle = atan2(enemypos.xy - ourpos.xy);
        order_weight = clamp(len_dist*OBEY_DIST_1, MIN_OBEY, 1.0);
      }
      if(cmd_id > 0.0)
      {
        //order_weight = 1.0;
        TCommand cmd = get_cmd(side, int(cmd_id-1.0));
        decision = decision*(1.0-order_weight) + ramp_decide(cmd.finish - ourpos.xy, data.xy, /*THRUST*/ourdata.y)*order_weight;
//        wantangle = atan2(command.xy - ourpos.xy);
//        wantangle = atan2(enemypos.xy - ourpos.xy);
      }


      float turnspeed = /*TURN_SPEED*/ourdata.z;
      float dangle = 0.0;
      float dist = angle_dist(wantangle, data.w);
      if(abs(dist) > /*ATTACK_ANGLE*/ourdata.w/2.0)
      {
        dangle = clamp(dist, -turnspeed, turnspeed);
      }
      //dangle = 0.05;

    /*float r = max(length(ourpos.xy), GRAVITY_HACK);
    r = r*r;*/
      gl_FragColor = data/**vec4(1.0-FRICTION, 1.0-FRICTION, 1.0, 1.0)*/ + vec4(decision, 0.0, dangle) /*- vec4(normalize(ourpos.xy)*GRAVITY/r , 0.0, 0.0)*/;
    }
  }
  else
  {
    vec4 data = texture2D(texdata, texcoord);
    vec2 position = data.xy;
    vec2 speed = texture2D(texdata, texcoord+vec2(1.0/SQRN,0.0)).xy;
    vec4 ourregen = shipdata(data.w, 1.0);

    if(data.z <= 0.0)
      data.z = max(data.z, HP_HIDE)-1.0;//burning
    else
    {
      vec2 hs = splithp(data.z);
      if(hs.x <= /*MAXHP*/ourregen.x)
        hs.x += ourregen.y;//repair
      if(hs.y <= /*MAXSHIELD*/ourregen.z)
        hs.y += ourregen.w;//shield regen
      data.z = combinehp(hs);
    }
    gl_FragColor = vec4(position+speed, data.zw);
  }


}
