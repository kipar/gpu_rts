
attribute float dummy;
varying vec4 color;
//x,y - rectangle coords. z - ship index(for offset), w - decay
varying vec4 texcoord;
uniform sampler2D texdata;
uniform sampler2D enemies;
uniform float draw_lines;

void main(void)
{
  float shipindex = floor(dummy/(6.0*4.0));
  float weaponindex = floor(mod(dummy/6.0,4.0));
  float vertexindex = floor(mod(dummy,6.0));
  if(vertexindex > 2.0)
    vertexindex -= 2.0;

  texcoord = vec4(floor(vertexindex/2.0), mod(vertexindex,2.0)-0.5, shipindex, 0.0);

  vec4 ourpos = texture2D(texdata, index2tex(shipindex*2.0));
  vec2 ta = texture2D(texdata, index2tex(shipindex*2.0+1.0)).zw;
  float target = floor(ta.x/MAX_COMMANDSf);
  float angle = ta.y;
  vec4 enemypos = texture2D(enemies, index2tex(target*2.0));

  vec2 position;

  float weapon1 = shipdata(ourpos.w, 2.0)[int(weaponindex)];
  float range = weapondata(weapon1, 0.0).w;
  //float reload = /*RELOAD*/weapondata(weapon1, 0.0).z;
  color =   weapondata(weapon1, 1.0);//*vertexindex;

  float wantangle = atan2(enemypos.xy - ourpos.xy);
  float fire_angle = /*ATTACK_ANGLE*/shipdata(ourpos.w, 0.0).w;

  float currange = length(ourpos.xy-enemypos.xy);



  //if(mod(shipindex, SHOW_LINES) != draw_lines || target < 0.0 || enemypos.z <= 0.0 || ourpos.z <= 0.0 || length(ourpos.xy-enemypos.xy) > get_param(FIRE_DISTANCE, ourpos.w) )
  if(weapon1<0.0 || target < 0.0 ||/* enemypos.z <= 0.0 || */ourpos.z <= 0.0 || currange > range || abs(angle_dist(wantangle, angle))>fire_angle)
  {
    position = vec2(0.0,0.0);
  }
  else
  {
    mat2 transform = mat2(cos(wantangle) , sin(wantangle), -sin(wantangle), cos(wantangle));
    position = ourpos.xy + transform*(texcoord.xy*vec2(currange, 0.0015*screen_scale) /*+ weaponindex*/);
    if (enemypos.z <= 0.0)
      texcoord.w = enemypos.z/HP_HIDE;
  }

  //else if (texcoord.x == 1.0)
  //  position = ourpos.xy;// /*SHIP_SIZE*/shipdata(ourpos.w, 0.0).xx*0.5;
  //else
 //   //position = command;
  //  position = enemypos.xy;//- shipdata(enemypos.w, 0.0).xx*0.5;

  gl_Position = our_to_screen(position);
}
