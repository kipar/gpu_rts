include: consts.hs
vertex: quad.vs
fragment: select_target.fs
uniform: check_offset: int
uniform: commands[80]: float
uniform: damage: sampler2D
uniform: enemies: sampler2D
uniform: order_side: int
uniform: statics: sampler2D
uniform: texdata: sampler2D
uniform: update_order: float
attribute: dummy: float
