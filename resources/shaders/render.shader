include: consts.hs
vertex: render.vs
fragment: render.fs
uniform: aspect: float
uniform: camera: vec2
uniform: cmd_colors[9]: vec4
uniform: screen_scale: float
uniform: side: int
uniform: statics: sampler2D
uniform: tex: sampler2D
uniform: texdata: sampler2D
attribute: dummy: float
