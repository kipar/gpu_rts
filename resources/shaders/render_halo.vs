
attribute float dummy;
uniform sampler2D texdata;
uniform sampler2D prevdata;
uniform int side;

varying vec2 relcoord;
varying vec2 thrust;
varying vec2 explosion;


void main(void)
{
  float shipindex = floor(dummy/6.0);
  float vertexindex = floor(mod(dummy,6.0));
  if(vertexindex > 2.0)
    vertexindex -= 2.0;

  vec4 data = texture2D(texdata, index2tex(shipindex*2.0));

  vec2 corner = vec2(floor(vertexindex/2.0), mod(vertexindex,2.0));

  float size = shipdata(data.w, 0.0).x*max(0.2, 0.00025*screen_scale) * 4.0;
  //float size = shipdata(data.w, 0.0).x*0.2* 4.0;

  vec4 data2 = texture2D(texdata, index2tex(shipindex*2.0+1.0));
  float angle = data2.w-pi_2;
  mat2 turn = mat2(cos(angle), sin(angle), -sin(angle), cos(angle));
  relcoord = corner-vec2(0.5);
  vec2 oldspeed = texture2D(prevdata, index2tex(shipindex*2.0+1.0)).xy;
  thrust = turn*(oldspeed - data2.xy)/shipdata(data.w, 0.0).y/*THRUST*/;//vec2(1.0);
  //thrust.x = - thrust.x;
  //dir = turn*vec2(0.0, 1.0);
  vec2 position = data.xy+turn*relcoord*size;
  gl_Position = our_to_screen(position);

  explosion = vec2(0.0);
  if(data.z<0.0)
  {
    if(data.z < HP_HIDE)
    {
      gl_Position = vec4(0.0);
    }
    else
    {
      float time = data.z/HP_HIDE;
      explosion = vec2(clamp(time*2.0, 0.0, 1.0), 1.0-time);
    }
  }
}
