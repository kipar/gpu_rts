
attribute float dummy;
varying vec2 texcoord;
varying vec2 rawtexcoord;
varying float hp;
varying float shield;
varying vec4 grid_color;
uniform sampler2D texdata;
uniform int side;


vec2 ship_tex(float side, float type)
{
  float index = side * ST_NTYPES + type;
  return vec2(mod(index, ATLAS_SIZE)/ATLAS_SIZE, floor(index/ATLAS_SIZE)/ATLAS_SIZE);
}

void main(void)
{
  float shipindex = floor(dummy/6.0);
  float vertexindex = floor(mod(dummy,6.0));
  if(vertexindex > 2.0)
    vertexindex -= 2.0;

  vec4 data = texture2D(texdata, index2tex(shipindex*2.0));

  vec2 corner = vec2(floor(vertexindex/2.0), mod(vertexindex,2.0));
  rawtexcoord = corner;
   texcoord = ship_tex(float(side-1), data.w) + corner/ATLAS_SIZE;

  float size = shipdata(data.w, 0.0).x*max(0.2, 0.0005*screen_scale);


  vec4 data2 = texture2D(texdata, index2tex(shipindex*2.0+1.0));
  float angle = data2.w;
  mat2 turn = mat2(cos(angle), sin(angle), -sin(angle), cos(angle));
  vec2 position = data.xy+turn*(corner-vec2(0.5))*size;

  vec4 regendata = shipdata(data.w, 1.0);

  vec2 hpshield = splithp(data.z);

  if(data.z>0.0)
    hp = min(hpshield.x/regendata.x, 1.0);
  else
    hp = data.z;

  shield = hpshield.y/regendata.z;

 int iindex = int(mod(data2.z, MAX_COMMANDSf));
 grid_color = get_cmd_color(iindex) * float(2-side) + vec4(1,0,0,1)*float(side-1);


  if(hp < HP_HIDE)
    gl_Position = vec4(0.0);
  else
    gl_Position = our_to_screen(position);
}
