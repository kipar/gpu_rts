attribute float dummy;
varying vec2 texcoord;
varying vec4 cmd_color;
uniform int side;

void main(void)
{
  TCommand cmd = get_cmd(side, int(dummy/6.0));
  cmd_color = get_cmd_color(int(dummy/6.0)+1);
  vec2 position;
  if(cmd.time < 0.0)
  {
    position = vec2(0);
  }
  else
  {
	  float vertexindex = floor(mod(dummy,6.0));
	  if(vertexindex > 2.0)
	  vertexindex -= 2.0;

	  texcoord = vec2(floor(vertexindex/2.0), mod(vertexindex,2.0));

    float angle = atan2(cmd.finish - cmd.start);
    float length = length(cmd.finish - cmd.start);
    mat2 transform = mat2(cos(angle) , sin(angle), -sin(angle), cos(angle));
    position = cmd.start + transform*((texcoord+vec2(0.0, -0.5))*vec2(length, screen_scale/100.0));

/*
    if(texcoord.x < 0.5)
      position = cmd.start+texcoord.y*5.0;
    else
      position = cmd.end+texcoord.y*5.0;*/
  }
	gl_Position = our_to_screen(position);
}
