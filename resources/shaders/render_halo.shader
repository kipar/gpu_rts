include: consts.hs
vertex: render_halo.vs
fragment: render_halo.fs
uniform: aspect: float
uniform: camera: vec2
uniform: prevdata: sampler2D
uniform: screen_scale: float
uniform: statics: sampler2D
uniform: texdata: sampler2D
attribute: dummy: float
