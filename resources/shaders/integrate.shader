include: consts.hs
vertex: quad.vs
fragment: integrate.fs
uniform: commands[80]: float
uniform: enemies: sampler2D
uniform: side: int
uniform: statics: sampler2D
uniform: texdata: sampler2D
attribute: dummy: float
