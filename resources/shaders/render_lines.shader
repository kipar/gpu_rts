include: consts.hs
vertex: render_lines.vs
fragment: render_lines.fs
uniform: aspect: float
uniform: camera: vec2
uniform: enemies: sampler2D
uniform: screen_scale: float
uniform: statics: sampler2D
uniform: texdata: sampler2D
uniform: time: float
attribute: dummy: float
