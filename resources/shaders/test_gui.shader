include: consts.hs
vertex: test_gui.vs
fragment: test_gui.fs
uniform: aspect: float
uniform: cursor: sampler2D
uniform: screen_scale: float
uniform: sunlight: vec3
uniform: sunpos: vec2
uniform: tex: sampler2D
attribute: dummy: float
