include: consts.hs
vertex: stats.vs
fragment: simple.fs
uniform: choosen: int
uniform: mouse: vec2
uniform: pass: int
uniform: screen_scale: float
uniform: statics: sampler2D
uniform: texdata: sampler2D
attribute: dummy: float
