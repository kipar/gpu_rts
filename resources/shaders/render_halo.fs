varying vec2 relcoord;
varying vec2 thrust;
varying vec2 explosion;
//varying vec2 dir;
//varying mat2 rot;

const float OFFSET = 0.1/8.0;
const vec2 leftpoint = vec2(-OFFSET, 0.0);
const vec2 rightpoint = vec2(OFFSET, 0.0);
const vec2 bottompoint = vec2(0.0, -OFFSET);
const vec2 toppoint = vec2(0.0, OFFSET);
const vec2 MAIN_THRUST = vec2(25.0, 3.0)/2.0;
const vec2 SIDE_THRUST = vec2(50.0, 5.0)/2.0;
const vec4 THRUST_COLOR = vec4(0.0, 1.0, 1.0, 1.0);
const vec4 MAINTHRUST_COLOR = vec4(1.0, 1.0, 0.5, 1.0);
const vec4 XPLODE_COLOR = vec4(1.0,0.6,0.3, 1.0);
const vec4 XPLODE_COLOR2 = vec4(1.0,0.3,0.3, 1.0);

void main(void)
{
  if(explosion.x > 0.0)
  {
	    float light = length(relcoord)*2.0;
      float r = rand2(relcoord);
      if(r>explosion.y)discard;
	    if(light > explosion.x-max(r*0.5, explosion.y)) discard;
/*	    if(light > explosion.x-r*1.5)
      {
  	    light = clamp(1.0-light/explosion.x, 0.0, 1.0);
        light = light*light*2.0;
        gl_FragColor = XPLODE_COLOR2*light*explosion.y;
        return;
      }*/
	    light = clamp(1.0-light/explosion.x, 0.0, 1.0);
      light = light*light*2.0;
	    gl_FragColor = XPLODE_COLOR*light;//*explosion.y;
  }
  else
  {
    gl_FragColor = vec4(0.0);
    //bottom thrust
      if(relcoord.y < 0.0 && thrust.y > 0.0)
      {
        float light = length((relcoord - bottompoint)*MAIN_THRUST);
        light = clamp(1.0-light, 0.0, 1.0);
        light = light*light*light;
        gl_FragColor = MAINTHRUST_COLOR*light*thrust.y;
      }
    //top thrust
      else if(relcoord.y > 0.0 && thrust.y < 0.0)
      {
        float light = min(length((relcoord - leftpoint)*SIDE_THRUST), length((relcoord - rightpoint)*SIDE_THRUST));
        light = clamp(1.0-light, 0.0, 1.0);
        light = light*light*light;
        gl_FragColor = THRUST_COLOR*light*(-thrust.y);
      }

    //left thrust
      if(relcoord.x < 0.0 && thrust.x > 0.0)
      {
        float light = min(length((relcoord - toppoint)*SIDE_THRUST.yx), length((relcoord - bottompoint)*SIDE_THRUST.yx));
        light = clamp(1.0-light, 0.0, 1.0);
        light = light*light*light;
        gl_FragColor = max(gl_FragColor, THRUST_COLOR*light*thrust.x);
      }
    //right thrust
      else if(relcoord.x > 0.0 && thrust.x < 0.0)
      {
        float light = min(length((relcoord - toppoint)*SIDE_THRUST.yx), length((relcoord - bottompoint)*SIDE_THRUST.yx));
        light = clamp(1.0-light, 0.0, 1.0);
        light = light*light*light;
        gl_FragColor = max(gl_FragColor, THRUST_COLOR*light*(-thrust.x));
      }
	if(gl_FragColor.a < 0.05)discard;
  if(rand2(relcoord) > gl_FragColor.a*10.0) discard;
  }
}
