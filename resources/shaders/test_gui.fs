varying vec2 texcoord;
varying vec2 starcoord;
uniform sampler2D tex;
uniform sampler2D cursor;
uniform vec3 sunlight;

const float CURSOR_SIZE = 0.02;

void main(void)
{
  float light = length(starcoord);
  light = clamp(1.0/light/light, 0.0, 1.0);
  //float r = snoise(starcoord);
  vec4 backcolor = texture2D(tex, texcoord);//vec4(1.0-step(r, 0.98));
  gl_FragColor = backcolor*(1.0-light)+vec4(sunlight, 1.0)*light;
  
  vec2 scr_coord = (texcoord - vec2(0.5, 0.5))*vec2(aspect, 1.0);
  if(length(scr_coord) < CURSOR_SIZE)
  {
    vec4 color = texture2D(cursor, scr_coord/CURSOR_SIZE+vec2(0.5));
    if(color.w > 0.1)
      gl_FragColor = color;
  }
}
