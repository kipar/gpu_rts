#version 110
const float SQRN = 256.0;
//uniform vec2 MAP_SIZE;
uniform vec2 camera;
uniform vec2 mouse;
uniform float screen_scale;
uniform float aspect;
uniform float time;
uniform sampler2D statics;
const float pi = 3.141592653589793238462643383279;
const float pi_2 = pi/2.0;
const float SHOW_LINES = 50.0;
const float STATIC_SIZE = 128.0;
const float STATS_SIZE = 8.0;
const float ATLAS_SIZE = 4.0;
const int BOARD_WEAPONS = 8;
const float ST_NWEAPONS = 10.0;
const float ST_NTYPES = 8.0;
const float ST_WEAPON_SIZE = 2.0;
const float ST_TYPE_SIZE = 4.0;
const float ST_TYPES_OFFSET = 0.0;
const float ST_WEAPONS_OFFSET = ST_TYPE_SIZE*ST_NTYPES;
const float ST_RANGES_OFFSET = ST_WEAPONS_OFFSET + ST_WEAPON_SIZE*ST_NWEAPONS;

const float PREDICT_MIN = 100.0;
const float PREDICT_D = 100.0;
const float K_ANGLE = 0.5;

const float MIN_OBEY = 0.1;
const float OBEY_DIST = 500.0;
const float OBEY_DIST_1 = 1.0/OBEY_DIST;
const float RAMP_K1 = 1e-0;
const float RAMP_K2 = 1e+1;


const int MAX_COMMANDS = 8;
const float MAX_COMMANDSf = float(MAX_COMMANDS);
const int COMMAND_SIZE = 5;

uniform vec4 cmd_colors[MAX_COMMANDS+1];

uniform float commands[2*MAX_COMMANDS*COMMAND_SIZE];

struct TCommand {
  vec2 start;
  vec2 finish;
  float time;
};
TCommand get_cmd(int side, int cmd_index)
{
  int index = (side-1)*MAX_COMMANDS+cmd_index;
  const int CMD_START = 0;
  const int CMD_END = 2;
  const int CMD_TIME = 4;
  return TCommand(vec2(commands[index*COMMAND_SIZE+CMD_START], commands[index*COMMAND_SIZE+CMD_START+1]),
    vec2(commands[index*COMMAND_SIZE+CMD_END], commands[index*COMMAND_SIZE+CMD_END+1]),
    commands[index*COMMAND_SIZE+CMD_TIME]
    );
}

vec4 get_cmd_color(int index)
{
 return cmd_colors[index];
}


const float HP_HIDE = -200.0;

//const float FRICTION = 0.0;
const float GRAVITY = 10.0;
const float GRAVITY_HACK = 200.0;
const int CHECK_PIECE = 1000;

vec2 index2tex(float index)
{
  return vec2(mod(index, SQRN)*(1.0/SQRN), floor(index/SQRN)*(1.0/SQRN));
}
vec2 index2static(float index)
{
  return vec2(mod(index, STATIC_SIZE)/STATIC_SIZE, floor(index/STATIC_SIZE)/STATIC_SIZE)+vec2(0.5/STATIC_SIZE);
}
vec2 index2stats(float index)
{
  return vec2(mod(index, STATS_SIZE)/STATS_SIZE, floor(index/STATS_SIZE)/STATS_SIZE)+vec2(0.5/STATS_SIZE);
}

vec4 weapondata(float weapon, float i)
{
  return texture2D(statics, index2static(ST_WEAPONS_OFFSET + weapon*ST_WEAPON_SIZE+i));
}
vec4 shipdata(float type, float i)
{
  return texture2D(statics, index2static(ST_TYPES_OFFSET + type*ST_TYPE_SIZE+i));
}
float optimal_range(float ourtype, float enemytype)
{
  float index = ST_RANGES_OFFSET + floor((ourtype*ST_NTYPES + enemytype)/4.0);
  int comp = int(mod(ourtype*ST_NTYPES + enemytype, 4.0));
  //return 100.0;
  return texture2D(statics, index2static(index))[comp];
}

vec4 our_to_screen(vec2 position)
{
  return vec4(2.0*((position-camera)/screen_scale / vec2(aspect, 1.0)),0,1);
}
vec4 abs_to_screen(vec2 position)
{
  return vec4(2.0*(position-0.5),0,1);
}


float rand(vec2 co){
  return fract(sin(dot(co,vec2(12.9898,78.233))) * 43758.5453);
}

vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 mod289(vec4 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 permute(vec4 x) {
     return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(vec4 r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}

float snoise(vec3 v)
  {
  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
  const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

// First corner
  vec3 i  = floor(v + dot(v, C.yyy) );
  vec3 x0 =   v - i + dot(i, C.xxx) ;

// Other corners
  vec3 g = step(x0.yzx, x0.xyz);
  vec3 l = 1.0 - g;
  vec3 i1 = min( g.xyz, l.zxy );
  vec3 i2 = max( g.xyz, l.zxy );

  //   x0 = x0 - 0.0 + 0.0 * C.xxx;
  //   x1 = x0 - i1  + 1.0 * C.xxx;
  //   x2 = x0 - i2  + 2.0 * C.xxx;
  //   x3 = x0 - 1.0 + 3.0 * C.xxx;
  vec3 x1 = x0 - i1 + C.xxx;
  vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y
  vec3 x3 = x0 - D.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y

// Permutations
  i = mod289(i);
  vec4 p = permute( permute( permute(
             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
           + i.y + vec4(0.0, i1.y, i2.y, 1.0 ))
           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

// Gradients: 7x7 points over a square, mapped onto an octahedron.
// The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)
  float n_ = 0.142857142857; // 1.0/7.0
  vec3  ns = n_ * D.wyz - D.xzx;

  vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)

  vec4 x_ = floor(j * ns.z);
  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

  vec4 x = x_ *ns.x + ns.yyyy;
  vec4 y = y_ *ns.x + ns.yyyy;
  vec4 h = 1.0 - abs(x) - abs(y);

  vec4 b0 = vec4( x.xy, y.xy );
  vec4 b1 = vec4( x.zw, y.zw );

  //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;
  //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;
  vec4 s0 = floor(b0)*2.0 + 1.0;
  vec4 s1 = floor(b1)*2.0 + 1.0;
  vec4 sh = -step(h, vec4(0.0));

  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
  vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

  vec3 p0 = vec3(a0.xy,h.x);
  vec3 p1 = vec3(a0.zw,h.y);
  vec3 p2 = vec3(a1.xy,h.z);
  vec3 p3 = vec3(a1.zw,h.w);

//Normalise gradients
  vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
  p0 *= norm.x;
  p1 *= norm.y;
  p2 *= norm.z;
  p3 *= norm.w;

// Mix final noise value
  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
  m = m * m;
  return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1),
                                dot(p2,x2), dot(p3,x3) ) );
  }

float rand2(vec2 co)
{
 return rand(co);//snoise(vec3(co*1111.1, 0.1*time));
}

float atan2(vec2 delta)
{
  return atan(delta.y, delta.x);
}
vec2 splithp(float hpshield)
{
  if(hpshield < 0.0)
    return vec2(hpshield, 0.0);
  vec2 result;
  result.x = floor(hpshield)/256.0;
  result.y = fract(hpshield)*64.0;

  return result;
}
float combinehp(vec2 hpshield)
{
  if(hpshield.x < 0.0)
    return hpshield.x;
//  return hpshield.x*256.0;
//limit to 256.0 fractions
  return floor(hpshield.x*256.0)+hpshield.y/64.0;

}

bool check_reload(float reload)
{
  return mod(time, reload)<0.1;
}


float angle_dist(float a1, float a2)
{
  return mod(a1 - a2 + pi, 2.0*pi) - pi;
}

