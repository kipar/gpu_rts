include: consts.hs
vertex: render_commands.vs
fragment: render_commands.fs
uniform: arrow: sampler2D
uniform: aspect: float
uniform: camera: vec2
uniform: cmd_colors[9]: vec4
uniform: commands[80]: float
uniform: screen_scale: float
uniform: side: int
attribute: dummy: float
