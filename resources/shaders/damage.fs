varying float attack;

void main(void)
{
  if(attack <= 0.0) discard;
  gl_FragColor = vec4(attack,0,0,1.0);
}
