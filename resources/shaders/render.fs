varying vec2 texcoord;
varying vec2 rawtexcoord;
varying float hp;
varying float shield;
varying vec4 grid_color;
uniform sampler2D tex;
uniform int side;


const vec2 FRAME = vec2(0.1, 0.25)/10000.0;

void main(void)
{
/*  {vec4 picture = texture2D(tex, texcoord);
  if(picture.w<=0.1) discard;
  if(hp <= HP_HIDE)discard;
  gl_FragColor = vec4(0);
  }
  return;*/

  //get to one quadrant
  if(grid_color.w > 0.0)
  {
	  vec2 curframe = FRAME*max(screen_scale, 10000.0);
	  vec2 c = 1.0-2.0*abs(rawtexcoord-vec2(0.5));
	  if(hp >= 0.0 && ((c.x < curframe.x && c.y < curframe.y) || (c.x < curframe.y && c.y < curframe.x)))
	  {
	  gl_FragColor = grid_color;//vec4(0.0,1.0,0.0,1.0);
	  return;
	  }
  }

  vec4 picture = texture2D(tex, texcoord);

  gl_FragColor = picture;
  if(picture.w<=0.1) discard;
  if(hp <= HP_HIDE)discard;
  if(hp <= 0.0)
  {
    float fade = (hp+100.0)/100.0 + rand2(texcoord)/2.0;
    if(fade < 0.3)
      discard;
    else if(fade < 0.7)
      gl_FragColor *= vec4(fade, fade, fade, 1)*0.2;
    else if(fade<1.0)
      gl_FragColor = vec4(1.0,0.6,0.3,1);
  }
  else if (hp < 0.5)
  {
      if(rand2(texcoord)*10.0<hp)
        gl_FragColor += vec4(1.0,0.6,0.3,1);
  }

  if (shield > 0.0)
      gl_FragColor += vec4(0.0, 0.0, shield/2.0, 0.0);
}
