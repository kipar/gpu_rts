varying vec2 texcoord;
uniform sampler2D texdata;
uniform sampler2D enemies;
uniform sampler2D damage;
uniform int check_offset;
uniform int order_side;
uniform float update_order;

void main(void)
{
  vec4 data = texture2D(texdata, texcoord);
  vec4 prev = texture2D(texdata, texcoord-vec2(1.0/SQRN, 0));
  if(mod(texcoord.x, 2.0/SQRN) <= 0.9/SQRN)
  {
    float hit = texture2D(damage, texcoord).x;
    vec2 hp = splithp(data.z);
    if(hit > hp.y)
      data.z = combinehp(vec2(hp.x-(hit-hp.y),0.0));
    else
      data.z = combinehp(vec2(hp.x, hp.y-hit));
  }
  else if(prev.z > 0.0)
  {
    //data -= texture2D(damage, texcoord-vec2(1.0/SQRN)).zzxz;
    vec2 ourpos = prev.xy;
    float target = floor(data.z/MAX_COMMANDSf);
    float com_id = mod(data.z, MAX_COMMANDSf);
    float ourangle = data.w;
    float fire_angle = /*ATTACK_ANGLE*/shipdata(prev.w, 0.0).w;
    vec3 enemypos = texture2D(enemies, index2tex(target*2.0)).xyz;
    float bestdist = (target >= 0.0 && enemypos.z > 0.0) ? length(enemypos.xy - ourpos)*K_ANGLE : 1e10;//TODO angle
    //float bestdist = 1e10;

    //search enemy
    for(int i = check_offset; i<check_offset+CHECK_PIECE; i++)
    {
      vec4 checkpos = texture2D(enemies, index2tex(float(i)*2.0));
      if(checkpos.z > 0.0)
      {
        vec2 rel_pos = checkpos.xy - ourpos;
        float corr = 1.0+step(abs(angle_dist(atan2(rel_pos), ourangle)), fire_angle)*(K_ANGLE-1.0);
        float checkdist = length(rel_pos)*corr;//TODO - get_param(FIRE_DISTANCE, checkpos.w);
//        checkdist *= corr;
        if (checkdist < bestdist)
        {
          bestdist = checkdist;
          target = float(i);
        }
      }
    }

    //search order
    if(update_order > 0.0 && com_id == update_order)
      com_id = 0.0;
    if(com_id == 0.0)
    {
      //search nearest order among all
      bestdist = 1e10;
      for(int i=0; i<MAX_COMMANDS-1; i++)
      {
        TCommand cmd = get_cmd(order_side, i);
        float l = length(cmd.start- ourpos);
        if(cmd.time >= 0.0 && l < bestdist)
        {
          bestdist = l;
          com_id = float(i+1);
        }
      }
    }
    //set new order if closer
    else if(update_order > 0.0)
    {
      TCommand newcmd = get_cmd(order_side, int(update_order-1.0));
      float olddist = length(get_cmd(order_side, int(com_id-1.0)).start - ourpos);
      float newdist = length(newcmd.start - ourpos);
      if(newcmd.time >= 0.0 && newdist < olddist)
        com_id = update_order;
    }
//    com_id = MAX_COMMANDSf-1.0;
    //set updated order
    data.z = target*MAX_COMMANDSf+com_id;
  }
  gl_FragColor = data;
}
