
attribute float dummy;
varying vec4 input;
uniform sampler2D texdata;
uniform int pass;
uniform int choosen;

const float CURSOR_SIZE_2 = 0.01;


void main(void)
{
  vec4 data = texture2D(texdata, index2tex(dummy*2.0));
  vec4 nextdata = texture2D(texdata, index2tex(dummy*2.0+1.0));
  float target = floor(nextdata.z/MAX_COMMANDSf);
  float cmd_id = mod(nextdata.z, MAX_COMMANDSf);

  if(data.z < HP_HIDE)
  {
    gl_Position = vec4(1.0,1.0,0.0,1.0);
    input = vec4(0.0);
    return;
  }

  input = vec4(1.0/SQRN/SQRN*2.0);
  float index;
  switch(pass)
  {
   case 1:
      index = 0.0+data.w;
      break;
   case 2:
      index = 8.0+cmd_id;
      break;
   case 3:
      index = 16.0;
      break;
   case 4:
      //input = vec4(index2tex(dummy*2.0), 0.0, 0.0);
      input = vec4(dummy+1.0);
      float size = shipdata(data.w, 0.0).x*max(0.2, 0.0005*screen_scale);
      if(length(mouse - data.xy) < size+screen_scale*CURSOR_SIZE_2)
        index = 17.0;
      else
        index = 100.0;
      break;
   case 5:
      index = 18.0;
      if (abs(dummy+1.0 - float(choosen)) > 0.5)
        index = 100.0;
      input = data;
      break;
  }
  gl_Position = abs_to_screen(index2stats(index));
}
