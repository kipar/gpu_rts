attribute float dummy;
varying vec2 texcoord;
varying vec2 starcoord;
uniform vec2 sunpos;

void main(void)
{
    float vertexindex = floor(mod(dummy,6.0));
    if(vertexindex > 2.0)
	  vertexindex -= 2.0;
    texcoord = vec2(floor(vertexindex/2.0), mod(vertexindex,2.0));
	gl_Position = abs_to_screen(texcoord);
	starcoord = ((texcoord-vec2(0.5))*screen_scale* vec2(aspect, 1.0)/*+camera*/-sunpos)/100.0;
}
