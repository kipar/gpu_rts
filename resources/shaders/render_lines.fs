varying vec4 color;
varying vec4 texcoord;
uniform sampler2D tex;
uniform int side;


float period_time(float period)
{
  return mod(time+texcoord.z, period)/period;
}

void main(void)
{
  //gl_FragColor = texture2D(tex, texcoord);
  float alpha = 1.0;
  switch(int(color.w))
  {
  case 1:
    if(texcoord.x < texcoord.w*2.30)
      discard;
    if(abs(mod(period_time(23.0), 0.1)-mod(texcoord.x, 0.1)) > 0.01)
      discard;
    break;
  case 2:
    //if(texcoord.x < texcoord.w*8.50)
    //  discard;
//    if(abs(mod(period_time(85.0), 0.1)) > 0.05)
//      discard;
    alpha = (1.0-texcoord.x);
    break;
  case 3:
    if(texcoord.x < texcoord.w*8.50)
      discard;
    if(abs(mod(period_time(85.0), 0.5)-mod(texcoord.x, 0.5)) > 0.01)
      discard;
    break;
  }
  gl_FragColor = vec4(color.xyz*alpha, 1.0);
}
