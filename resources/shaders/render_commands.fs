varying vec2 texcoord;
varying vec4 cmd_color;
uniform sampler2D arrow;

void main(void)
{
  if(texture2D(arrow, texcoord).w<=0.1) discard;
	gl_FragColor = cmd_color;
}
