unit uGlobal;

{$mode objfpc}

interface

uses
  Classes, SysUtils, windows;


var
  Quitting :boolean = False;
  XSize, YSize: Integer;



  function keypressed(k :char) :boolean; overload;
  function keypressed(k :word) :boolean; overload;


  var
    // http://stackoverflow.com/questions/10535950/forcing-nvidia-gpu-programmatically-in-optimus-laptops
    // http://developer.download.nvidia.com/devzone/devcenter/gamegraphics/files/OptimusRenderingPolicies.pdf
    NvOptimusEnablement: uint32 = 1; export name 'NvOptimusEnablement';

implementation


function keypressed(k :char) :boolean; overload;
begin
  Result := GetAsyncKeyState(Ord(k)) <> 0;
end;

function keypressed(k :word) :boolean; overload;
begin
  Result := GetAsyncKeyState(k) <> 0;
end;




end.

