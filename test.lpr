program test;

{$APPTYPE CONSOLE }
{$MODE Delphi}

uses
  mymath in 'mymath.pas',
  uenginewrap in 'uEnginewrap.pas',
  SysUtils,
  uEngine,
  Resources,
  gl,
  GLext,
  uGlobal;

exports
  NvOptimusEnablement;

  //        __lock := True;
  //        SetCursorPos(XSize div 2, YSize div 2);
  //        __lock := False;

var
  oldx, oldy, scroll: TCoord;
  btnwasup: array[TMouseButton] of boolean;
  btnup: boolean;
  btn: TMouseButton;

begin
  try
    EngineSet(VSync, 0);
    EngineSet(Autoscale, 0);
    EngineSet(Antialias, 4);
    EngineInit('./resources');
    EngineSet(ClearColor, 0);
    if not Load_GL_VERSION_3_2() then
      log('Failed to load opengl');
    INIT;

    for btn := LeftButton to MiddleButton do
      btnwasup[btn] := true;

    oldx := MouseGet(CursorX);
    oldy := MouseGet(CursorY);
    repeat
      //glDisable(GL_DEPTH_TEST);
      glBlendFunc(GL_ONE, GL_ONE);
      //glBlendEquation(GL_FUNC_ADD);

      RENDER;
      PROCESS;
      RenderTo(THE_SCREEN);

      scroll := MouseGet(ScrollPos);
      if scroll <> 0 then
      begin
        if scroll > 0 then
          Game.Scale := Game.Scale / 1.1
        else
          Game.Scale := Game.Scale * 1.1;
      end;
      for btn := LeftButton to MiddleButton do
      begin
        btnup := MouseState(btn) = mbsUp;
        if btnup <> btnwasup[btn] then
        begin
          if btnup then
            MOUSEUP(Ord(btn) + 1, MouseGet(CursorX), MouseGet(CursorY))
          else
            MOUSEDOWN(Ord(btn) + 1, MouseGet(CursorX), MouseGet(CursorY));
          btnwasup[btn] := btnup;
        end;
      end;
      if (oldx <> MouseGet(CursorX)) or (oldy <> MouseGet(CursorY)) then
      begin
        MOUSEMOVE(Trunc(MouseGet(CursorX) - oldx), Trunc(MouseGet(CursorY) - oldy));
        oldx := MouseGet(CursorX);
        oldy := MouseGet(CursorY);
      end;
      ProcessScroll;
      ShaderActivate(DEFAULT_SHADER);
      ShowStats;
      EngineProcess;
    until (KeyState(KeyEscape) = ksPressed) or (KeyState(Quit) = ksPressed) or Quitting;
    DONE;
  except
    on E: Exception do
      log('Exception ' + E.ClassName + ' with message: ' + E.Message);
  end;
end.
