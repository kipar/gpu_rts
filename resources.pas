unit Resources;

interface
{$J-}
type
TRawResource = (res_Areas_dat, res_Army_dat, res_Research_dat, res_Science_dat, res_Ships_dat, res_Weapons_dat);
TSprite = (THE_SCREEN = -1, res_Arrow, res_Cursor, res_Ships);
TSound = (NO_MUSIC = -1, res_Music);
TButton = (TButton_NOT_USED);
TTileMap = (res_Ship_icons);
TFont = (res_Consola);
TShader = (ALL_SHADERS = -1, DEFAULT_SHADER = 0, res_Damage, res_Integrate, res_Render, res_Render_commands, res_Render_halo, res_Render_lines, res_Select_target, res_Stats, res_Test_gui);

TShaderAttribute = (attr_color, attr_pos, attr_texpos, attr_dummy);
TShaderUniform = (uni_screen_size, uni_tex, uni_enemies, uni_statics, uni_texdata, uni_time, uni_commands, uni_side, uni_aspect, uni_camera, uni_cmd_colors, uni_screen_scale, uni_arrow, uni_prevdata, uni_check_offset, uni_damage, uni_order_side, uni_update_order, uni_choosen, uni_mouse, uni_pass, uni_cursor, uni_sunlight, uni_sunpos);


TData = record
  Areas_dat: TRawResource;
  Army_dat: TRawResource;
  Research_dat: TRawResource;
  Science_dat: TRawResource;
  Ships_dat: TRawResource;
  Weapons_dat: TRawResource;
end;

TShaders = record
  Damage: TShader;
  Integrate: TShader;
  Render: TShader;
  Render_commands: TShader;
  Render_halo: TShader;
  Render_lines: TShader;
  Select_target: TShader;
  Stats: TShader;
  Test_gui: TShader;
end;

TRES = record
  Data: TData;
  Shaders: TShaders;
  Arrow: TSprite;
  Consola: TFont;
  Cursor: TSprite;
  Music: TSound;
  Ships: TSprite;
  Ship_icons: TTileMap;
end;

const RES: TRES = (
  Data: (
    Areas_dat: res_Areas_dat;
    Army_dat: res_Army_dat;
    Research_dat: res_Research_dat;
    Science_dat: res_Science_dat;
    Ships_dat: res_Ships_dat;
    Weapons_dat: res_Weapons_dat;
  );
  Shaders: (
    Damage: res_Damage;
    Integrate: res_Integrate;
    Render: res_Render;
    Render_commands: res_Render_commands;
    Render_halo: res_Render_halo;
    Render_lines: res_Render_lines;
    Select_target: res_Select_target;
    Stats: res_Stats;
    Test_gui: res_Test_gui;
  );
  Arrow: res_Arrow;
  Consola: res_Consola;
  Cursor: res_Cursor;
  Music: res_Music;
  Ships: res_Ships;
  Ship_icons: res_Ship_icons;
);

implementation
end.
