unit uTextures;

interface

uses
  Classes, SysUtils, gl, uEngine, Resources;

type

  { TTexture }

  TTexture = class
    raw: uEngine.TTexture;
    SqrN: integer;
    constructor Create(aSqrN: integer);
    procedure ToGPU(fromdata: Pointer);
    procedure FromGPU(todata: Pointer);
    procedure ActivateReading(sh: TShader; uni: TShaderUniform);
    procedure ActivateWriting;
    constructor CreateStars;
  end;

const
  TEX_FULL = 1024;

implementation

constructor TTexture.CreateStars;
var
  buffer: array[1..TEX_FULL, 1..TEX_FULL, 1..4] of byte;
  I, J, k: integer;
  c: array[1..4] of byte;
  x, y: single;
  basex, basey: single;
begin
  Create(TEX_FULL);
  FillChar(buffer, sizeof(buffer), 0);
  basex := random;
  basey := random;
  for i := 1 to 20 do
  begin
    FillChar(c, sizeof(c), 255);
    for k := 1 to 1 do
      c[random(4) + 1] := 155;
    x := random;
    y := random;
    for J := 1 to 100 do
    begin
      x := abs(frac(x + cos(y + basey)));
      y := abs(frac(y + sin(x + basex)));
      for k := 1 to 4 do
        buffer[trunc(x * (TEX_FULL - 1)) + 1, trunc(y * (TEX_FULL - 1)) + 1, k] := c[k];
    end;
  end;
  TextureLoadPixels(raw, @buffer, AsByte);
end;

{ TTexture }

constructor TTexture.Create(aSqrN: integer);
begin
  SqrN := aSqrN;
  raw := TextureCreate(SqrN, SqrN);
end;

procedure TTexture.ToGPU(fromdata: Pointer);
begin
  TextureLoadPixels(raw, fromdata, AsFloat);
end;

procedure TTexture.FromGPU(todata: Pointer);
var
  p: Pointer;
  f: single;
begin
  p := TextureGetPixels(raw, @f, @f, AsFloat);
  Move(p^, todata^, SqrN * SqrN * 16);
end;

procedure TTexture.ActivateReading(sh: TShader; uni: TShaderUniform);
begin
  UniformSetTexture(sh, uni, raw);
end;

procedure TTexture.ActivateWriting;
begin
  RenderTo(raw);
end;

end.
